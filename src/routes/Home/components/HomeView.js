import React from 'react'
import PropTypes from 'prop-types'
import './HomeView.scss'
import { connect } from 'react-redux';
import WSService from '../../../services/wsService'
import LoadedService from '../../../services/loadedService'
import { autoSignin, doSignout } from "../../../store/server";
let QRCode = require('qrcode.react');
import logo from '../assets/logo.png';

class HomeView extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  };

constructor (props) {
    super(props)
  this.state = {
    value: '',
    backloaded:false,
    size: 256,
    fgColor: '#333333',
    bgColor: '#ffffff',
    level: 'L',
    tid:null,
    userPresent:false,
    udpEnabled:false,
    loadedService: null,
    server_version: null,
  };
}

 getIp = async () => {
   	const res = await fetch(`http://${location.hostname}:3001/`);
   	if (!res.ok) throw new Error("unable to get server info");
   	return await res.json();
  }
  componentDidUpdate(){
    console.log(this.state);
  }

  componentDidMount() {
    console.log("mounting HomeView")
      if(this.props.lastScreen) {
        console.log("existing connection");
        this.props.socketServer.changeOnMessage(this.onServerMessage);
        // this.setState({
        //   value: "192.168.1.14:3001",
        //   loadedService : new LoadedService("192.168.1.14:3001", null)
        // })
      }
      else {
        console.log("new connection");
        this.getIp().then(({ server_ip, server_url, server_version }) => {

          this.setState({
            value: server_ip + ":3001",
            loadedService: new LoadedService('http://localhost:3001', () => {
              this.props.autoSignin("desktop@pc.it", "cppotksed", this.onServerMessage, console.log, server_url)
            }),
            server_version,
          })
        }).catch(err => setTimeout(this.reloadPage, 2000));
      }
  }


   componentWillUnmount(){
     //this.props.doSignout();
  }

  reloadPage = () => {
    window.location.reload();
  }




onServerMessage = (e) => {
    const event = JSON.parse(e.data)
    console.log("HomeView onServerMessage", event);
    const identifier = event.identifier ? JSON.parse(event.identifier) : {};
    const message = event.message ? JSON.parse(event.message) : {};
    //console.log("CardsView onServerMessage content", identifier, message);

    switch( event.type){
      case 'confirm_subscription':
        // Save the channel I'm subscribed t
        this.props.socketServer.subscribedChannel = identifier.channel;
      break;

      case 'ping':
        return;

      default:
        break;
    }

    if(message && message.type == "USER_CONNECTED"){

      //this.props.doSignout();
      if(this.props.lastScreen){
        console.log("back to last screen", this.props.lastScreen)
        this.context.router.replace(process.env.BASEURL+this.props.lastScreen);

      }
      else{
        this.context.router.replace(process.env.BASEURL+'/cards');
      }
    }
  else if(message && message.type == "USER_ON_NETWORK"){
      console.log("user on network");
    this.setState({userPresent:true})
  }
  }


    render(){
    console.log("should I draw", this.state.loadedService && this.state.loadedService.loaded && (this.state.userPresent || !this.state.udpEnabled))

          //{this.state.value.length > 0 && <div className="spinner"></div>}
         if (this.state.loadedService && this.state.loadedService.loaded && (this.state.userPresent || !this.state.udpEnabled)) {
          return (<div className="logo-container">
            <img src={logo}/>
            <div className="qr-section">
          <QRCode
            value={this.state.value}
            size={this.state.size}
            fgColor={this.state.fgColor}
            bgColor={this.state.bgColor}
            level={this.state.level}/>
          <div className="advices">
          <h2>Scansiona il codice QR con l'app per continuare</h2>
            <p>Hai problemi con la connessione?</p>
            <ul>
              <li>assicurati che il telefono e il computer siano collegati alla stessa rete</li>
              <li>assicurati che il telefono abbia inquadrato correttamente il codice QR (dovresti sentire una vibrazione di conferma)</li>
              <li>se hai ancora problemi, prova a ricaricare questa pagina</li>
            </ul>
            <button className="reload-button" onClick={this.reloadPage}>
              RICARICA
            </button>
          </div>
            </div>
            {this.props.lastScreen && <div className={"pauseAdvice"}>
              <p><strong>Sayeye è in pausa. </strong>L'app mobile non è attiva sul telefono o è stata chiusa. Riporta l'app Sayeye in primo piano, o accedi di nuovo inquadrando il codice QR o premendo il tasto "salta" (se disponibile).</p><p>Se per ora hai finito di usare Sayeye, chiudi questa finestra. </p>
            </div>}
            <div className="home-version-info">
              <b>versione client:</b> <code>{process.env.VERSION || '(unknown)'}</code><br />
              <b>versione server:</b> <code>{this.state.server_version || '(unknown)'}</code>
            </div>
          </div>)
          }
          else if(!this.state.userPresent && this.state.udpEnabled){

            return (<div className="logo-container">
                <img src={logo}/>

              <div className="wait-container">
                <div className="lds-ripple">
                  <div></div>
                  <div></div>
                </div>
                <p>Sto cercando dispositivi mobile Sayeye. Accertati che computer e telefono siano connessi alla stessa rete Wifi e che l'app sia attiva.</p>
            </div>
              <div className="home-version-info">
                <b>versione client:</b> <code>{process.env.VERSION || '(unknown)'}</code>
              </div>
            </div>)

          }
         else{
           return(<div className="logo-container">
             <img src={logo}/>
             <div className="home-version-info">
               <b>versione client:</b> <code>{process.env.VERSION || '(unknown)'}</code>
             </div>
           </div>)
         }
    }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.server.currentUser,
    currentPatient: state.server.currentPatient,
    socketServer: state.server.socketServer,
    socketEyeServer: state.server.nodeSocketServer,
    lastScreen: state.flow.lastScreen,
    serverUrl: state.server.serverUrl
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    autoSignin: (email, password, onServerMessage, onMobileMessage, serverip) => {dispatch(autoSignin(email, password, onServerMessage, onMobileMessage,serverip))},
    doSignout: () => {dispatch(doSignout())},
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeView);
